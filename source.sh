# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/linux-pam/linux-pam/releases/download/v${BPM_PKG_VERSION}/Linux-PAM-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  ./configure --prefix=/usr                        \
              --sbindir=/usr/sbin                  \
              --sysconfdir=/etc                    \
              --libdir=/usr/lib                    \
              --enable-securedir=/usr/lib/security \
              --docdir=/usr/share/doc/Linux-PAM
  make
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  chmod -v 4755 "$BPM_OUTPUT"/usr/sbin/unix_chkpwd
  mkdir -p "$BPM_OUTPUT"/etc/pam.d/
  cp ../system-account "$BPM_OUTPUT"/etc/pam.d/system-account
  cp ../system-auth "$BPM_OUTPUT"/etc/pam.d/system-auth
  cp ../system-session "$BPM_OUTPUT"/etc/pam.d/system-session
  cp ../system-password "$BPM_OUTPUT"/etc/pam.d/system-password
  cp ../system-user "$BPM_OUTPUT"/etc/pam.d/system-user
  cp ../other "$BPM_OUTPUT"/etc/pam.d/other
}
